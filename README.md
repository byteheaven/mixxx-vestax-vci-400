# mixxx-vestax-vci-400

## Note: no longer maintained
This controller mapping was not updated for Mixxx 2.2. You can use it if you are
still using Mixxx 1.x, but it is recommended you switch to the project
bhvn-mixxx-vci400 for Mixxx v2.2 and higher.

## Description
Vestax VCI-400 mapping for Mixxx, based on the official mapping, with bug fixes and 
some additional mappings.

For more info, see the mixxx project (under res/controllers) and the official mixxx.org
website.

